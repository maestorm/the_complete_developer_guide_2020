import axios from 'axios';

interface ToDo {
  id: number,
  title: string,
  completed: boolean
}

const url = 'https://jsonplaceholder.typicode.com/todos/4';
axios.get(url).then(response => {
  const todo = response.data as ToDo;
  const id = todo.id;
  const title = todo.title;
  const completed = todo.completed;

  logToDo(id, title, completed);
});

const logToDo = (id: number, title: string, completed: boolean) => {
  console.log(`
    The todo with id: ${id}
    has title: ${title}
    is it finished: ${completed}
  `);
}